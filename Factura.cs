using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEBER DE POO
{
    public class Factura
    {
        public string Observacion { get; set; }
        public int IdCliente { get; set; }
        public int Numero { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Total { get; set; }
    }
}
