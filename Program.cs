using System;
using System.Collections.Generic;
using System.Linq;

namespace DEBER DE POO
{
    class Program
    {
        static List<Cliente> ListaCliente;
        static List<Factura> ListaFactura;
        static void Main(string[] args)
        {
        ListaCliente = new List<Cliente>
                        {
                        new Cliente {Nombre= "mario", IdCliente= 3482 },
                        new Cliente {Nombre= "jose", IdCliente= 2155 },
                        new Cliente {Nombre= "Ariel", IdCliente= 2156 },
                        new Cliente {Nombre= "esteven", IdCliente= 8291 },
                        new Cliente {Nombre= "belen", IdCliente= 2910 },
                        new Cliente {Nombre= "lucia", IdCliente= 1929 },
                        new Cliente {Nombre= "joselin", IdCliente= 9382 },
                        new Cliente {Nombre= "kevin", IdCliente= 8291 },
                        new Cliente {Nombre= "jostin", IdCliente= 1272 },
                        new Cliente {Nombre= "roberto", IdCliente= 1289 },
                        };
            ListaFactura = new List<Factura>
                        {
                        new Factura {Observacion= "Prob", IdCliente= 3482, Fecha=new DateTime (2020, 21, 12),  Total=100, Numero= 1 },
                        new Factura {Observacion= "Prob", IdCliente= 2155, Fecha=new DateTime (2017, 26, 04), Total=2352, Numero= 2 },
                        new Factura {Observacion= "Prob", IdCliente= 2156, Fecha=new DateTime (2013, 28, 06), Total=293, Numero= 3 },
                        new Factura {Observacion= "Prob", IdCliente= 8291,  Fecha=new DateTime (2010, 28, 03), Total=333, Numero= 4 },
                        new Factura {Observacion= "Estás", IdCliente= 2910, Fecha=new DateTime (2012, 26, 04), Total=1020, Numero= 5 },
                        new Factura {Observacion= "Compra", IdCliente= 1929, Fecha=new DateTime (2004, 22, 11), Total=456, Numero= 6 },
                        new Factura {Observacion= "Observación", IdCliente= 9382, Fecha=new DateTime (2017, 07, 08), Total=500, Numero= 7 },
                        new Factura {Observacion= "Prob", IdCliente= 8291, Fecha=new DateTime (2012, 12, 151), Total=200, Numero= 8 },
                        new Factura {Observacion= "Hola", IdCliente= 1272,  Fecha=new DateTime (2013, 02, 12), Total=203, Numero= 9},
                        new Factura {Observacion= "Prob", IdCliente= 1289, Fecha=new DateTime (2014, 04, 03), Total=1000, Numero= 10 },
                        };
            int opcion = 0;

            do
            {
                Console.WriteLine("que desea realizar");
                Console.WriteLine("\n" +

                Console.WriteLine("sleccione una opcion :");
                opcion = Convert.ToInt32(Console.ReadLine());
                switch (opcion)
                {
                 "\n 1.-  Mostrar en consola todos los números primos." +
                    case 1:
                        Console.Clear();
                        var PrimosQuery = from numepri in Numeros
                                          where numpri % 2 != 0
                                          select numpri;
                        foreach (var Num in PrimosQuery)
                        {
                            Console.WriteLine("mostrar los numeros primos: " + Num);
                        }
                        break;
                    "\n 2.-  Mostrar suma de todos los elementos." +
                    case 2:
                        Console.Clear();
                        IEnumerable<int> sumaQuery = from numero in Numeros
                                                     select numero;
                        Console.WriteLine("mostrar la suma de todos los elementos:  " + sumaQuery.Sum());
                        break;
                    "\n 3.-  Generar una nueva lista con el cuadrado de los números." +
                    case 3:
                        Console.Clear();
                        var nuevoformlista = from nuevalis in Numeros
                                         where nuevalis > 0
                                         select nuevalis;
                        var nuevoformlis = (from nuevalis in Numeros
                                          where nuevalis > 0
                                          select nuevalis).ToArray();

                        Console.Write("Los números son: ");

                        foreach (var cuadrado in nuevoformlista)
                        {
                            Console.Write(cuadrado + ",");
                        }
                        Console.Write("\n");
                        Console.Write("este es el numero cuadrado: ");

                        foreach (var cuadrado in nuevoformlista)
                        {
                            Console.Write(Math.Pow(cuadrado, 2) + ",");
                        }
                        Console.WriteLine("\n");
                        break;
                    "\n 4.-  generar nueva lista de los numeros primos ." +
                    case 4:
                        Console.Clear();
                        var NumerosNPrimos = (from NuevalistaPrimos in Numeros
                                              where NuevoslistaPrimos % 2 != 0
                                              select NuevoslistaPrimos).ToArray();
                        foreach (var NuevosNumeros in NumerosNPrimos)
                        {
                            Console.Write(NuevosNumeros + ",");
                        }
                        Console.Write("\n");
                        break;
                    "\n 5.-  Obtener el promedio de todos los números mayores a 50." +
                    case 5:
                        Console.Clear();
                        IEnumerable<int> NumerMayor50 = from Numeromayor in Numeros
                                                         where Numeromayor > 50
                                                         select Numeromayor;
                        foreach (var Nmayor in NumeroMayor50)
                        {
                            Console.Write("Los números mayores a 50 son: ");
                            Console.Write(Nmayor + ",");
                        }
                        Console.WriteLine("\n");
                        Console.Write("El promedio de los números mayores a 50 es de  :" + NumeroMayor50.Average());
                        Console.WriteLine("\n");
                        break;
                    "\n 6.-  Contar la cantidad de números pares e impares." +
                    case 6:
                        Console.Clear();
                        var NumerosPar = from contanumeros in Numeros
                                         group contanumeros by (contanumeros % 2) == 0 into Total
                                         select Total;
                        foreach (var Numeropar in NumerosPar)
                        {
                            if (Numeropar.Key)
                            {
                                Console.WriteLine("La cantidad de números pares es de : " + NumerosPar.Count());
                            }
                            if (!Numeropar.Key)
                            {
                                Console.WriteLine("La cantidad de números impares es de : " + NumerosPar.Count());
                            }
                        }
                        Console.WriteLine("\n");
                        break;
                    "\n 7.-  Mostrar en consola, el número y la cantidad de veces que este se encuentra en la lista. " +
                    case 7:
                        Console.Clear();
                        var numerosrepetidos =
                            from Numerosrepetidos in Numeros
                            group Numerosrepetidos by Numerosrepetidos into gruporep
                            select gruporep;
                        foreach (var item in numerosrepetidos)
                        {
                            Console.WriteLine("El número es:  " + item.Key + "  este se repite:   " + item.Count() + "   vez/veces.");
                        }
                        Console.WriteLine("\n");
                        break;
                    "\n 8.-  Mostrar en consola los elementos de forma descendente." +
                    case 8:
                        Console.Clear();
                        var DescendentesNumeros =
                            from numerodescendentes in Numeros
                            orderby numerodescendentes descending
                            select numerodescendentes;

                        Console.Write("En orden descendentes los números quedan en el siguiente orden: ");

                        foreach (var item in DescendentesNumeros)
                        {
                            Console.Write(+item + ",");
                        }
                        Console.WriteLine("\n");
                        break;
                    "\n 9.-  Mostrar en consola los números únicos." +
                   
                    case 9:
                        Console.Clear();
                        var Numerosunicos =
                            from Numunico in Numeros
                            group Numunico by Numunico into Numunicotot
                            where Numunicotot.Count() == 1
                            select Numunicotot;

                        Console.Write("Los números únicos son: ");

                        foreach (var Num in Numerosunicos)
                        {
                            Console.Write(Num.Key + ",");
                        }
                        Console.WriteLine("\n");
                        break;
                   "\n 10.- Sumar todos los números únicos de la lista." +
                   
                    case 10:
                        Console.Clear();
                        int SumaUnicos = 0;
                        var numerosTotalUnicos =
                            from numerosunico in Numeros
                            group numerosunico by numerosunico into sumaGroup
                            where sumaGroup.Count() == 1
                            select sumaGroup;
                        foreach (var suma in numerosTotalUnicos)
                        {
                            SumaUnicos += suma.Key;
                        }
                        Console.WriteLine("muestre total de la suma de numeros unicos: " + SumaUnicos);
                        Console.WriteLine("\n");
                        break;
                  
                  
                    case 11:
                        Console.Write(

                          "\n 11.- Pasar al proceso de búsqueda de cliente y factura" +

                          "         consulta del cliente y de la factura       \n\n\n" +
                         
                        Console.WriteLine("----Presione la tecla enter si desa continuar-----");
                        Console.ReadKey();
                        Console.WriteLine("----muestre los 3 clientes con más monto en ventas");
                        var Tresmontoventa =
                            (from cliente in ListaCliente
                             join factura in ListaFactura
                             on cliente.IdCliente equals factura.IdCliente
                             orderby factura.Total descending

                             select new { cliente.Nombre, factura.Total }).Take(3);
                        Console.WriteLine("Los tres mayores montos de venta son :");
                        foreach (var item in Tresmontoventa)
                        {
                            {
                                Console.WriteLine("El nombre del cliente es: " + item.Nombre + "  con un total de : " + item.Total);
                            }
                        }
                        Console.WriteLine("\n");
                        Console.WriteLine("Mostrar en consola los 3 clientes con menos monto en ventas.");
                        var Tresmontoventamenor = (from cliente in ListaCliente
                                                   join factura in ListaFactura
                                                   on cliente.IdCliente equals factura.IdCliente
                                                   orderby factura.Total ascending
                                                   select new { cliente.Nombre, factura.Total }).Take(3);
                        Console.WriteLine("Los tres menores montos de venta son :");
                        foreach (var item in Tresmontoventamenor)
                        {
                            {
                                Console.WriteLine("El nombre del cliente es : " + item.Nombre + " con un total de : " + item.Total);
                            }
                        }
                        Console.WriteLine("\n");
                        Console.WriteLine("Muestre la consola  cliente con más ventas realizadas");
                        var Clientemasventa = (from cliente in ListaCliente
                                               join factura in ListaFactura
                                               on cliente.IdCliente equals factura.IdCliente
                                               where cliente.IdCliente == factura.IdCliente
                                               select new { cliente.IdCliente, cliente.Nombre, factura.Numero }).Take(2);
                        foreach (var item in Clientemasventa)
                        {
                            {
                                Console.WriteLine("cliente con más venta es : " + item.Nombre + " con número de venta : " + item.Numero);
                            }
                        }
                        Console.WriteLine("\n");
                        Console.WriteLine("Mostrar en consola el cliente y la cantidad de ventas realizadas");
                        var ventasrealizadas = from ventas in ListaFactura
                                               join cliente in ListaCliente
                                               on ventas.IdCliente equals cliente.IdCliente
                                               where ventas.Fecha < new DateTime(2018, 02, 03)
                                               select new { cliente.Nombre, ventas.Fecha };
                        foreach (var item in ventasrealizadas)
                        {
                            {
                                Console.WriteLine("Ventas del cliente : " + item.Nombre + "  con dos ventas realizadas " );
                            }
                        }
                        Console.WriteLine("\n");
                        Console.WriteLine("Mostrar en consola las ventas realizadas hace menos de 1 año.");
                        var Ventashaceunaño = from ventas in ListaFactura
                                              join cliente in ListaCliente
                                              on ventas.IdCliente equals cliente.IdCliente
                                              where ventas.Fecha > new DateTime(2021, 04, 01)
                                              select new { cliente.Nombre, ventas.Fecha };
                        foreach (var item in Ventashaceunaño)
                        {
                            {
                                Console.WriteLine("Venta del cliente : " + item.Nombre + "  con fecha del  : " + item.Fecha);
                            }
                        }
                        Console.WriteLine("\n");
                        Console.WriteLine("Mostrar en consola las ventas más antiguas");
                        var Ventamasantigua = from ventas in ListaFactura
                                              join cliente in ListaCliente
                                              on ventas.IdCliente equals cliente.IdCliente
                                              where ventas.Fecha < new DateTime(2017, 04, 01)
                                              select new { cliente.Nombre, ventas.Fecha };
                        foreach (var item in Ventamasantigua)
                        {
                            {
                                Console.WriteLine("ventas más antigua es de : " + item.Nombre + " con fecha de : " + item.Fecha);
                            }
                        }
                        Console.WriteLine("\n");
                        Console.WriteLine("•	Mostrar en consola los clientes que tengan una venta cuya observación comience con Prob);
                        var observaciones = ListaFactura.Where(ListaFactura => ListaFactura.Observacion.Contains("Prob", StringComparison.CurrentCultureIgnoreCase));
                        foreach (var item in observaciones)
                        {
                            {
                                Console.WriteLine(item.Observacion + " ID del cliente :" + item.IdCliente);
                            }
                        }
                        break;
               "\n 12.- Salir del progrma\n");
                    case 12:
                        Console.WriteLine("Fin.muestre los resultados");
                        break;
                    default:
                        break;
                }
            } while (opcion != 12) ;
            Console.ReadKey();
        }
    }
}
